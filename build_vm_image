#!/bin/bash
set -xe

PACKER_JSON=centos-6.6.json
if [ -z $1 ]; then
   S3_BUCKET="s3://centos-6-vmdk"
else
   S3_BUCKET=$1
fi

function validate() {
   PACKER_CMD=`which packer` 
   if [[ -z $PACKER_CMD ]]; then
      help
      exit 1
   fi

   VDISKMANAGER_CMD=`PATH=$PATH:$VMWARE_PATH which vmware-vdiskmanager`
   if [[ -z $VDISKMANAGER_CMD ]]; then
      help
      exit 1
   fi

   AWS_CMD=`which aws`
   if [[ -z $AWS_CMD ]]; then
      help
      exit 1
   fi
}

function help() {
    echo "Usage: build_vm_image [s3://bucket]"
    echo "This script expects the `packer`, `aws`, and `berkshelf` commands to be \
       in \$PATH. Refer to README.md for instructions on installing packer and \
       berkshelf"
    echo "This script also expects the `vdiskmanager` command to be in \$PATH or
    in \$VMWARE_PATH"
    exit 1
}

function installBerks() {
   local cur_dir=$(pwd)
   find $cur_dir -name Berksfile| while read file; do
      cd `dirname $file`
      berks vendor $cur_dir/vendor/cookbooks
   done
}

function cleanUp() {
   rm -fr `pwd`/vendor
}

function buildPackerImg() { 
   $PACKER_CMD build -force -only=virtualbox-iso $PACKER_JSON
}

#Convert VMDK v3 image produced by packer into a raw VMDK image using
#vmware-vdiskmanager
function convertVmdkToRawVmdk() {
   find `pwd` -name *.vmdk.new | xargs rm -f
   PATH=$PATH:$VMWARE_PATH find `pwd` -name *.vmdk -exec vmware-vdiskmanager -r {} -t 0 {}.new \; 
   for file in $(find `pwd` -name *.vmdk.new); do
      local orig_file=`echo $file | rev | cut -d. -f2- | rev`
      rm -f $orig_file
      mv $file $orig_file
   done
}

function uploadToS3() {
   for vmdk in $(find `pwd` -name *.vmdk); do
      local filename=`echo $vmdk | rev | cut -d/ -f 1 | rev`
      $AWS_CMD s3 cp $vmdk $S3_BUCKET/$filename
   done
   for ovf in $(find `pwd` -name *ovf); do
      local filename=`echo $ovf | rev | cut -d/ -f 1 | rev`
      $AWS_CMD s3 cp $ovf $S3_BUCKET/$filename
   done
}

function main() {
   validate
   cleanUp
   installBerks
   buildPackerImg
   convertVmdkToRawVmdk
   uploadToS3
}

main
