This repository contains packer scripts to build a OVF template containing 
CentOS 6.6.

# Project Structure

~~~~~~~~~~~
http/ks.cfg - Kickstarter config file that will configure your CentOS 6.6 image
centos-6.6.json - Packer config file
~~~~~~~~~~~

# Build Instructions

## Build Virtualbox OVF template

### Pre-requisites

* Virtualbox
* Ruby
* Packer
* [Future] Berksfile

### Build Instructions

~~~~~~~~~~~
git clone https://siddhuwarrier@bitbucket.org/siddhuwarrier/centos-packer.git
cd centos-packer
packer build -only=virtualbox-iso centos-6.6.json
~~~~~~~~~~~

This will create a headless virtualbox instance using a CentOS ISO downloaded
from the Internet, and run kickstart to configure it. The created virtualbox 
image may be found in the `output-virtualbox-iso` directory.

### Where can the OVF file be used?

The OVF file can be imported into:

* Virtualbox
* VMware Fusion
* VSphere Client connected to VSphere hypervisor (untested)

## Build VMWare `vmx` file

This can only be done on a desktop machine (support for ESX to follow).

### Pre-requisites

To build a VMware `vmx` file, you will need to run this on a node with:
* A GUI
* VMWare Player (Linux), VMware Fusion (Mac OS X), or VMWare Workstation (Windows)
* Ruby
* Packer

### Build

~~~~~~~~
cd centos-packer
packer build -only=vmware-iso
~~~~~~~~

If you installed VMware Fusion using brew cask, set the environment variable
`FUSION_APP_PATH` to your VMWare Fusion installation directory (in 
`~/Applications/VMware\ Fusion.app`) before running `packer build`.

# Troubleshooting

## Building VMware images
- If you receive the error `Error detecting host IP: exit status 1` when 
building on a Mac, perform the following steps:
  - Reboot OS X (I'm not entirely sure why, but the `vmnet8` interface began to
appear on ifconfig after I performed this and the steps below; this step may be unnecessary)
  - Run the following commands:
~~~~~~~~~~~~~~~~~~~~~
    $ sudo rm -f "Library/Preferences/VMware Fusion/locations"
    $ sudo rm -f "Library/Preferences/VMware Fusion/networking"
    $ sudo /Applications/VMware\ Fusion.app/Contents/Library/vmnet-cli -c
    $ sudo /Applications/VMware\ Fusion.app/Contents/Library/vmnet-cli --status #Make sure vmnet8 is running, as Packer uses this.
    DHCP service on vmnet1 is running
    Hostonly virtual adapter on vmnet1 is enabled
    DHCP service on vmnet8 is running
    NAT service on vmnet8 is running
    Hostonly virtual adapter on vmnet8 is enabled
    All the services configured on all the networks are running
~~~~~~~~~~~~~~~~~~~~~
- Do not change the `guest_os_type`. It took 1 hour of staring at completely 
bizzare (and unrelated) errors to realise that the guest_os_type for Centos is `redhat`.

# TODO

- Change ISO URL to use a US one so that latency from Jenkins box isn't too 
high (assuming Jenkins is in us-east-1)
- What timezone do we want the VM to be in? I would imagine UTC is a good pick 
given the global nature of the customer base. (Set at us-eastern right now)
- Review the packages installed by kickstarter.
- Add support for headless vmware package builds using ESXi (on AWS using 
Ravello)

# To Review

## Parameters

Parameter | Location | Notes |
--------- | -------- | ----- |
`ssh_wait_timeout` | `http/ks.cfg`| Currently unset and defaults to 5m0s; review whether they should be raised
`timezone` |  `http/ks.cfg` | Currently set to US/Eastern; review whether this should be switched to UTC
`rootpw`| `http/ks.cfg` | Currently set. Do we even need this set?
`disk_size` | `centos-6.6.json` | Currently set to 20480M; review 

## Other choices

* Centos 6.6 chosen instead of Centos 6.5, as no security updates will be released for 6.5 going forward.
* Should the partitions be encrypted?
* Passwordless sudo for lockhart user required to complete VM build using Packer. Disable on provisioning using Chef?
