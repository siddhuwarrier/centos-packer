# Bridge VM cookbook

Chef cookbook to prepare a Bridge VM. The Bridge VM contains:

* A Docker installation
* A linux service that will on first boot build a LAR Docker image with the
unique key inside the VMDK.


## Supported Platforms 

CentOS 6.6

## Usage

### SSHing on to the LAR container

To SSH on to the LAR container, use the `docker_ssh` private key in
`/root/docker/src`. This key is generated uniquely for each LAR image created.

## License and Authors

Author:: Siddhu Warrier <siwarrie@cisco.com>
