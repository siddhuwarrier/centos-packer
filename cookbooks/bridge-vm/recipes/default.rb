package 'libcgroup'

service 'cgconfig' do
    action [:enable, :start]
end

docker_service 'default' do
    action [:create, :start]
end

directory '/root/docker' do
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end

remote_directory '/root/docker/docker_config' do
  source 'docker_config'
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end

cookbook_file '/root/docker/Dockerfile' do
  source 'Dockerfile'
  owner 'root'
  group 'root'
  mode '0600'
end

cookbook_file '/usr/local/bin/build-lar-img' do
  source 'build-lar-img'
  owner 'root'
  group 'root'
  mode '0755'
end

cookbook_file '/usr/local/bin/run-lar' do
  source 'run-lar'
  owner 'root'
  group 'root'
  mode '0755'
end

cookbook_file '/etc/init.d/lar' do
  source 'lar'
  owner 'root'
  group 'root'
  mode '0755'
end

service 'lar' do
  action :enable
end
