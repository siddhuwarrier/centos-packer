#!/bin/bash
#
#       /etc/init.d/build_docker_img
#
#       This builds a LAR image on first boot. The service is then disabled in
#       chkconfig, but can be run manually.
#    
# chkconfig:   345 95 95
# description: Daemon for building LAR docker image

### BEGIN INIT INFO
# Provides:       LAR image builder
# Required-Start: $network
# Required-Stop:
# Should-Start:
# Should-Stop:
# Default-Start: 3 4 5
# Default-Stop:  0 1 6
# Short-Description: Build a LAR docker image if one doesn't exist, and start up the LAR container
# Description: Daemon for LAR
### END INIT INFO   
. /etc/init.d/functions

BUILD_SCRIPT=/usr/local/bin/build-lar-img
RUN_SCRIPT=/usr/local/bin/run-lar
BUILDER_LOG_FILE="/var/log/build-lar-img.log"
SERVICE_NAME="lar"

function start() {
  $BUILD_SCRIPT status
  if [ $? -eq 1 ]; then
    action "Building LAR image [output logged to $BUILDER_LOG_FILE]:" $BUILD_SCRIPT build
  fi

  action "Starting LAR container:" $RUN_SCRIPT run
}

function status() {
  action "LAR image built:" $BUILD_SCRIPT status
  action "LAR container running:" $RUN_SCRIPT status 
}

function stop() {
  action "Stopping LAR container:" $RUN_SCRIPT stop 
}

case "$1" in
  start)
    start
    ;;
  status)
    status
    ;;
  stop)
    stop
    ;;
  *)
    echo "Usage: service lars [start|stop|status]"
    ;;
esac 
